import { wordToGuess } from "./App.js";

let errorTryNumber = 12;

const wordToGuessArray = wordToGuess.toLowerCase().split('');
// console.log(wordToGuessArray);

let resultArray = [];

for (let i = 0; i < wordToGuess.length; i++) {
    resultArray[i] = '_';
    
}

function tryLetter (letter) {
    letter = letter.toLowerCase();

    if (!wordToGuessArray.includes(letter)) {
        errorTryNumber--;
        return;
    }
    
    for (let i = 0; i < wordToGuessArray.length; i++) {
        if (letter === wordToGuessArray[i]) {
            resultArray[i] = letter;
        }
    }
    console.log(resultArray.join(''));
    console.log("Plus que " + errorTryNumber + " essais \n");
}

// while(wordToGuessArray !== resultArray && errorTryNumber > 0) {
//     // const response = 
    tryLetter('J');
    tryLetter('a');
    tryLetter('v');
    tryLetter('a');
    tryLetter('s');
    tryLetter('c');
    tryLetter('u');
    tryLetter('r');
    tryLetter('b');
    tryLetter('i');
    tryLetter('p');
    tryLetter('t');
// }

